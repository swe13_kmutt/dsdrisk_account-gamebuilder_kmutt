/**
 * Created by iscode on 5/18/17.
 */
import spark.Request;
import spark.Response;
import spark.Route;
import static spark.Spark.*;
public class Main {
    public static void main(String[] args) {
        get("/Login", new Route() {
            public Object handle(Request request, Response response) {
                return JsonUtil.toJson(  UserService.Login("1234","4567") );
            }
        });
        get("/CreateAccount", new Route() {
            public Object handle(Request request, Response response) {
                return JsonUtil.toJson(  UserService.createAccount("Supachai","4567") );
            }
        });
        get("/Logout", new Route() {
            public Object handle(Request request, Response response) {
                return JsonUtil.toJson(  UserService.Logout("12345") );
            }
        });

    }
}
