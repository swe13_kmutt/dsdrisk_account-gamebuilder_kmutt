import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 * Created by iscode on 5/18/17.
 */
public class JsonUtil {
    public static String toJson(Object object) {
        return new Gson().toJson(object);
    }

    public static ResponseTransformer json() {
        return JsonUtil.json();
    }
}
